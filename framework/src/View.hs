{-# LANGUAGE RecordWildCards #-}

module View (
    draw
) where

import Graphics.Gloss
import Graphics.Gloss.Geometry.Angle
import Graphics.Gloss.Data.Vector
import Data.Time.Clock
import GHC.Float

import Model
import Controller.Time

-- | Drawing

-- Function that draws all elements to screen.
draw :: Float -> Float -> World -> Picture
draw h v world = pictures (multiplier : score : player : trail ++ explosion ++ bonuses ++ enemyList ++ bullets ++ stars)
    where player     = drawPlayer             world
          enemyList  = drawEnemy              world
          score      = drawScore              world
          multiplier = drawMultiplier         world
          bullets    = drawBullets            world
          bonuses    = drawBonuses            world
          explosion  = drawExplosionParticles world 
          trail      = drawTrailParticles     world
          stars      = drawStarParticles      world

-- All the following functions are, based on the current Game World state, written to correcly represent all game objects in a Picture
--  or list of Pictures.

drawPlayer :: World -> Picture
drawPlayer world = playerBody rot (playerPosition world)
    where rot = radToDeg (argV (playerVelocity world)) * (-1) + 90 -- rotation of player picture is based its current velocity.

drawEnemy :: World -> [Picture]
drawEnemy world = drawEnemies (enemies world)
    where drawEnemies []      = []
          drawEnemies (x:xs)  = enemyBody (posEnemy x) (scaleY x) : drawEnemies xs
          scaleY x            = (sin (scaleyEnemy x) + 2) * 0.5 -- Returns a value between 0.5 and 1.5. Sin function gives a wave-like motion.

drawScore :: World -> Picture
drawScore world = scoreText (score world)

drawMultiplier :: World -> Picture
drawMultiplier world = multiplierText (multiplier world)

drawBullets :: World -> [Picture]
drawBullets world = drawBullet (bullets world)
    where drawBullet []     = []
          drawBullet (x:xs) = bulletBody (fst x) : drawBullet xs

drawBonuses :: World -> [Picture]
drawBonuses world = drawBonus (bonuses world)
    where drawBonus []     = []
          drawBonus (x:xs) = bonusBody (fst x) (scale(snd x)) : drawBonus xs
          scale x          = 0.25 * (sin x) + 1.25 -- Returns a value between 1 and 1.5. Sin function gives a wave-like motion.

drawExplosionParticles :: World -> [Picture]
drawExplosionParticles world = drawPart (explosion world)
    where drawPart []     = []
          drawPart (x:xs) = explosionParticle (posPart x) : drawPart xs

drawTrailParticles :: World -> [Picture]
drawTrailParticles world = drawPart (trail world)
    where drawPart []     = []
          drawPart (x:xs) = trailParticle (posPart x) : drawPart xs

drawStarParticles :: World -> [Picture]
drawStarParticles world = drawPart (stars world)
    where drawPart []     = []
          drawPart (x:xs) = starParticle (sizePart x) (posPart x) : drawPart xs





