{-# LANGUAGE DisambiguateRecordFields, NamedFieldPuns #-}

module Model where

import System.Random
import Graphics.Gloss
import Graphics.Gloss.Data.Picture
import Data.Time.Clock

import GHC.Float

-- | Game state

data World = World {
        -- Time
        startTime        :: UTCTime,
        time             :: UTCTime,
        -- Random generator
        rndGen           :: StdGen,
        -- Event queue
        rotateAction     :: RotateAction,
        movementAction   :: MovementAction,
        shootAction      :: ShootAction,
        -- Player
        playerPosition   :: Position,
        playerVelocity   :: Velocity,
        playerAccel      :: Float,
        bullets          :: [Bullet],
        -- Enemy
        timer            :: Int,
        enemies          :: [Enemy],
        enemiesDied      :: [(Bool, Bullet)],
        -- Particles
        trail            :: [Particle],
        explosion        :: [Particle],
        stars            :: [Particle],
        -- Bonus
        bonuses          :: [Bonus],
        -- Score
        score            :: Int,
        multiplier       :: Int
    }

type Position       = (Float, Float)
type Velocity       = (Float, Float)
type Rotation       = Float
type Size           = Float
type Speed          = Float
type Drag           = Float
type Life           = Int

type Bullet         = (Position, Velocity)
type Enemy          = (Velocity, Position, Size)
type Bonus          = (Position, Size)
type Particle       = (Position, Velocity, Speed, Drag, Life, Size)

-- Extract Enemy properties, using the following functions:
velEnemy    (a, _, _) = a
posEnemy    (_, b, _) = b
scaleyEnemy (_, _, c) = c

-- Extract Particle properties, using the following functions:
posPart   (a,_,_,_,_,_) = a
velPart   (_,b,_,_,_,_) = b
speedPart (_,_,c,_,_,_) = c
dragPart  (_,_,_,d,_,_) = d
lifePart  (_,_,_,_,e,_) = e
sizePart  (_,_,_,_,_,f) = f

data RotateAction   = NoRotation | RotateLeft | RotateRight
data MovementAction = NoMovement | Thrust
data ShootAction    = Shoot      | DontShoot
data ParticleType   = Explosion  | StarField  | Trail

-- Returns the initialised game World.
initial :: UTCTime -> World
initial seed = World{startTime      = seed, 
                     time           = seed, 
                     rndGen         = mkStdGen 0, 
                     rotateAction   = NoRotation, 
                     movementAction = NoMovement, 
                     shootAction    = DontShoot, 
                     playerPosition = (0, 0), 
                     playerVelocity = (1, 0), 
                     playerAccel    = 1, 
                     bullets        = [], 
                     timer          = 0, 
                     enemies        = [], 
                     enemiesDied    = [], 
                     trail          = [(playerPosition (initial seed), (0,0), 0, 0, 0, 1)], 
                     explosion      = [], 
                     stars          = [], 
                     bonuses        = [], 
                     score          = 0, 
                     multiplier     = 1}

-- All the following functions calculate a Picture which is used to present a game object on screen, in its correct size / position /
--  rotation.

playerBody :: Rotation -> Position -> Picture
playerBody rot (px,py) = Translate px py $ Rotate rot $ color red $ Polygon[(-6, -9), (6, -9), (0, 9)]

enemyBody :: Position -> Size -> Picture
enemyBody (px,py) sizeY = Translate px py $ Scale 1 sizeY $ color c $ body
    where body = Polygon[(-13, 0), (0, -13), (13, 0), (0, 13)]
          c    = makeColor 0 1 1 1

bulletBody :: Position -> Picture
bulletBody (px,py) = Translate px py $ Color yellow $ circleSolid 2

bonusBody :: Position -> Size -> Picture
bonusBody (px, py) s = Translate px py $ Scale s s $ Color green $ circleSolid 10

scoreText :: Int -> Picture
scoreText score = Translate (-450) (230) $ Scale 0.3 0.3 $ Color white $ Text ("Score: " ++ (show score))

multiplierText :: Int -> Picture
multiplierText multiplier = Translate (230) (230) $ Scale 0.3 0.3 $ Color white $ Text ("Multiplier: " ++ (show multiplier))

explosionParticle :: Position -> Picture
explosionParticle (px, py) = Translate px py $ Color orange $ circleSolid 1

trailParticle :: Position -> Picture
trailParticle (px, py) = Translate px py $ Color red $ circleSolid 1

starParticle :: Size -> Position -> Picture
starParticle s (px, py) = Translate px py $ Color white $ circleSolid s





