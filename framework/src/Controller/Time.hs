{-# LANGUAGE DisambiguateRecordFields, NamedFieldPuns, RecordWildCards       #-}
{-# LANGUAGE ParallelListComp                                                #-}

module Controller.Time (
    timeHandler
) where

import Control.Arrow ((>>>))

import Data.Time.Clock.POSIX (getPOSIXTime)
import Data.List
import Data.Time.Clock
import GHC.Float

import Graphics.Gloss
import Graphics.Gloss.Geometry.Angle
import Graphics.Gloss.Data.Vector

import System.Random

import Model

-- | Time handling

----------------------------- UPDATE FUNCTION -----------------------

-- Returns an updated version of a given world. It's called 60 times a second. 
timeHandler :: Float -> World -> World
timeHandler dt world | playerDies world = resetWorld (time world) world
                     | otherwise        = let (newRndGen:g0:g1:g2:_) = unfoldr (Just . split) (rndGen world)
                                          in 
                                          world {time            = addUTCTime (realToFrac dt) $ time world,
                                                 rndGen          = newRndGen,
                                                 playerAccel     = updatePlayerAccel (rotateAction world) world,
                                                 playerVelocity  = updatePlayerVelocity world dt, 
                                                 playerPosition  = updatePlayerPosition dt world,
                                                 timer           = (timer world) + 1,
                                                 enemies         = if   not (timer world `mod` 20 == 0) 
                                                                   then updateEnemy (enemies world) dt world -- enemy spawned every 3rd of a second.
                                                                   else spawnEnemy g0 : updateEnemy (enemies world) dt world,
                                                 enemiesDied     = updateDiedEnemies world,
                                                 bullets         = updateBullet (spawnBullet world (shootAction world)) dt world,
                                                 bonuses         = if   not (timer world `mod` 120 == 0)
                                                                   then updateBonuses world -- bonus spawned every 2 seconds.
                                                                   else spawnBonus g1 : updateBonuses world,
                                                 score           = updateScore world,
                                                 multiplier      = updateMultiplier world,
                                                 trail           = updateParticles Trail     (trail     world) dt world,
                                                 explosion       = updateParticles Explosion (explosion world) dt world,
                                                 stars           = fst (spawnParticle StarField world g2) : 
                                                                   updateParticles StarField (stars     world) dt world
                                                }

---------------------- RANDOM -----------------------
randomPosition :: StdGen -> Float -> Float -> Position
randomPosition rand hor vert = (px, py)
    where r1 = randomR (-hor, hor) rand
          r2 = randomR (-vert, vert) (snd r1)
          px = fst r1
          py = fst r2

randomVelocity :: StdGen -> Float -> Velocity
randomVelocity rand speed = mulSV speed (normalizeV(vx, vy))
    where r1 = randomR ((-1.0), 1.0) rand
          r2 = randomR ((-1.0), 1.0) (snd r1)
          vx = fst r1
          vy = fst r2

randomFloat :: StdGen -> Float -> Float -> Float
randomFloat rand min max = fst $ randomR (min, max) rand

randomLife :: StdGen -> Int
randomLife rand = fst $ randomR (80, 120) rand

-------------------- RESET WORLD --------------------

-- Returns the World variables to their default values. It also triggers an explosion, creates new random generators and gives the player
--  a new random position and rotation.
resetWorld :: UTCTime -> World -> World
resetWorld time world = let (newGen:g0:g1:_)   = unfoldr (Just . split) (rndGen world)
                            new                = initial time
                        in  new{rndGen         = newGen,
                                playerPosition = randomPosition g0 300 100,
                                playerAccel    = randomFloat    g1 0    10, -- note that playerAccel determines the player's rotation
                                explosion      = spawnExplosion world ++ (explosion world),
                                stars          = stars world}

-------------------- PLAYER -------------------------

-- Returns the player's speed, depending on its current MovementAction.
playerSpeed :: MovementAction -> Float
playerSpeed NoMovement = 300
playerSpeed Thrust     = 500

-- Updates the player's position. This is done by adding the velocity to the current position of the player. If the player reached the upper
--  boundary of the game world, the y-position of the player will not be updated. If the player reaches the side bounds, the x-position will not
--  be updated.
updatePlayerPosition :: Float -> World -> Position
updatePlayerPosition dt world | reachesXBound && reachesYBound = (fst(playerPosition world), snd(playerPosition world))
                              | reachesXBound                  = (fst(playerPosition world), py')
                              | reachesYBound                  = (px', snd(playerPosition world))
                              | otherwise                      = (px', py')
    where px'           = (fst (playerPosition world)) + (fst (playerVelocity world)) * dt
          py'           = (snd (playerPosition world)) + (snd (playerVelocity world)) * dt
          nextXPos      = fst  (playerPosition world)  + fst  (playerVelocity world)  * dt
          nextYPos      = snd  (playerPosition world)  + snd  (playerVelocity world)  * dt
          reachesXBound = nextXPos <= -500 || nextXPos >= 500
          reachesYBound = nextYPos <= -270 || nextYPos >= 270

-- Returns the updated velocity of the player. This is calculated by feeding the player acceleration into a sin- and cos function for the
--  x- and y-velocity respectively. It is then multiplied by the player's speed.
updatePlayerVelocity :: World -> Float -> Velocity
updatePlayerVelocity world dt = (vx', vy')
      where vx' = sin (playerAccel world) * (playerSpeed (movementAction world))
            vy' = cos (playerAccel world) * (playerSpeed (movementAction world))

-- Returns the updated player acceleration, depending on the current RotateAction. This value will be fed into sin-/cos-function to rotate
--  the player correctly.
updatePlayerAccel :: RotateAction -> World -> Float
updatePlayerAccel RotateRight world = playerAccel world + 0.09
updatePlayerAccel RotateLeft  world = playerAccel world - 0.09
updatePlayerAccel _           world = playerAccel world

-- Returns True if the player collides with an enemy. 
playerDies :: World -> Bool
playerDies world = collision (enemies world)
    where playerPos        = playerPosition world
          posDistance x    = magV $ (posEnemy x) - playerPos
          collision []     = False
          collision (x:xs) = posDistance x < 12 || collision xs

-------------------- BULLET --------------------

-- Returns the speed of the bullet, which is double the speed of the player.
bulletSpeed :: World -> Float
bulletSpeed world = (playerSpeed (movementAction world)) * 2

-- Updates the bullet position. It removes a bullet from the bullet list if it killed an enemy or if it exceeded the boundaries of the
--  game world.
updateBullet :: [Bullet] -> Float -> World -> [Bullet]
updateBullet []     _ _      = []
updateBullet (x:xs) dt world | check x   = updateBullet xs dt world ++ newList
                             | otherwise = ((px', py'), (snd x)) : updateBullet xs dt world ++ newList
    where px'     = fst(fst x) + fst(snd x) * bulletSpeed world * dt
          py'     = snd(fst x) + snd(snd x) * bulletSpeed world * dt
          newList = []
          check b = (True,b) `elem` (enemiesDied world) || bulletDies b

-- Spawns a bullet at the player position with the player velocity, if the current ShootAction is Shoot.
spawnBullet :: World -> ShootAction -> [Bullet]
spawnBullet world DontShoot = bullets world
spawnBullet world     Shoot = (bulletPos, bulletVel) : (bullets world)
    where bulletPos = playerPosition world
          bulletVel = normalizeV $ playerVelocity world

-- Returns True if the bullet exceeded the boundaries of the game world.
bulletDies :: Bullet -> Bool
bulletDies bullet = horiBounds || vertBounds
    where horiBounds = fst(fst bullet) < (-500) || fst(fst bullet) > 500
          vertBounds = snd(fst bullet) < (-300) || snd(fst bullet) > 300

-------------------- ENEMY --------------------

-- A constant value which is used to slow the enemy down.
enemyDrag :: Float
enemyDrag = 0.5

-- Returns a value which is fed into a Sin function. This is then used to scale the Y size of the enemy, resulting in a jellyfish-like motion.
updateEnemyScaleY :: Size -> Size
updateEnemyScaleY enemyY = enemyY + 0.05

-- Returns the updated position of a given enemy. It is calculated by adding the velocity of the enemy to its current position.
updateEnemyPosition :: Float -> Enemy -> Position
updateEnemyPosition dt enemy = updatePos enemy
    where updatePos x = (px' x, py' x)
          px'       e = (fst (posEnemy e)) + (fst (velEnemy e)) * dt
          py'       e = (snd (posEnemy e)) + (snd (velEnemy e)) * dt

-- Returns the updated velocity of a given enemy. It is calculated by subtracting the player position vector by the enemy's position.
--  It is then multiplied by enemyDrag, to slow down the enemy.
updateEnemyVelocity :: Float -> Enemy -> World -> Velocity
updateEnemyVelocity dt enemy world = updateVel enemy
    where updateVel (a,b,c) = mulSV enemyDrag $ (playerPosition world) - b

-- Uses the previous 'update' enemy functions to return the updated versions of all the enemies in the current game World.
updateEnemy :: [Enemy] -> Float -> World -> [Enemy]
updateEnemy []     _  _ = []
updateEnemy (x:xs) dt world | fst $ enemyDies (bullets world) x = (updateEnemy xs dt world) ++ enemyList
                            | otherwise                         = (a,b,c) : (updateEnemy xs dt world) ++ enemyList
    where a         = updateEnemyVelocity dt x world
          b         = updateEnemyPosition dt x
          c         = updateEnemyScaleY (scaleyEnemy x)
          enemyList = []

-- Spawns an enemy at a random position.
spawnEnemy :: StdGen -> Enemy
spawnEnemy rand = ((0,0), randomPosition rand 420 250, 1)

-- Returns a tuple whose first element indicates if a given enemy has 'died'. Its second element returns the bullet that killed the enemy.
enemyDies :: [Bullet] -> Enemy -> (Bool, Bullet)
enemyDies bullets enemy = check bullets
    where check []     = (False, (0,0))
          check (x:xs) | (dist x) < 20 = (True, x)
                       | otherwise     = check xs
          dist b       = magV $ (fst b) - (posEnemy enemy)

-- Returns a list of all enemies that have died from a bullet in the form of (True, *bullet*).
updateDiedEnemies :: World -> [(Bool, Bullet)]
updateDiedEnemies world = filter ((==True).fst) list
    where list = map (enemyDies (bullets world)) (enemies world)

-------------------- PARTICLES --------------------

-- Updates particle velocity, by multiplying its previous velocity by its drag factor (function only used for explosion particles).
updateParticleVelocity :: Particle -> Velocity
updateParticleVelocity p = mulSV (dragPart p) (velPart p)

-- Updates particle position, by adding its velocity to its current position. 
updateParticlePosition :: Float -> Particle -> World -> Position
updateParticlePosition dt p world = (px', py')
    where px' = fst(posPart p) + fst(velPart p) * dt
          py' = snd(posPart p) + snd(velPart p) * dt

updateParticles :: ParticleType -> [Particle] -> Float -> World -> [Particle]
-- If the ParticleType is a StarField, then it first checks if it exceeds the right edge of the screen. If not, the particle is updated,
--  using updateParticlePosition.
updateParticles StarField []     _  _     = []
updateParticles StarField (x:xs) dt world | fst(posPart x) > 500 = updateParticles StarField xs dt world
                                          | otherwise            = updatedPart : updateParticles StarField xs dt world
    where pos'        = updateParticlePosition dt x world
          updatedPart = (pos', velPart x, speedPart x, 1, 0, sizePart x)

-- If the ParticleType is a Trail, then it first checks if there are already 10 Trail particles in the Game World. If so, it adds a new
--  particle with the player's position, and removes the last Trail particle. If not, it does not remove the last Trail particle.
updateParticles Trail     (x:xs) dt world | length(x:xs) == 10 = newPart : init xs
                                          | otherwise          = newPart : (x:xs)
    where newPart = (playerPosition world, (0,0), 0, 0, 0,1)

-- If the ParticleType is an Explosion, then it first checks if the life of a particle is smaller then 0. If so, it removes this particle from the 
--  explosion list. If not, it updates the particle position / velocity and adds the updated particle to the explosion list.
updateParticles Explosion []     _  _     = []
updateParticles Explosion (x:xs) dt world | lifePart x < 0 = updateParticles Explosion xs dt world
                                          | otherwise      = updatedPart : updateParticles Explosion xs dt world
    where pos'        = updateParticlePosition dt x world
          vel'        = updateParticleVelocity x
          updatedPart = (pos', vel', speedPart x, dragPart x, lifePart x - 1, 1)

spawnParticle :: ParticleType -> World -> StdGen -> (Particle, StdGen)
-- If the ParticleType is a StarField, it creates a new particle with a random position (left from the screen). Its speed is calculated
--  using its size, which is also randomly decided (smaller particles move slower).
spawnParticle StarField world seed = ((pos, (1 * speed,0), speed, 1, 0, size), snd (split seed))
    where (g0:g1:g2:g3:_) = unfoldr (Just . split) seed
          pos             = (px', py')
          px'             = randomFloat g0 (-600) (-500)
          py'             = randomFloat g1 (-250) 250
          speed           = size * (randomFloat g2 100 300)
          size            = randomFloat g3 0.1 1.3

-- If the ParticleType is an Explosion, it creates a new particle, whose position is equal to the player's position. Its, velocity, speed,
--  drag and life are all random, resulting in a more realistic explosion.
spawnParticle Explosion world seed = (((playerPosition world), vel, speed, drag, life, size), snd (split seed))
    where (g0:g1:g2:g3:_) = unfoldr (Just . split) seed
          vel             = randomVelocity g0 speed
          speed           = randomFloat    g1 300  650
          drag            = randomFloat    g2 0.91 0.96
          life            = randomLife     g3
          size            = 1

-- Spawns 500 particles, created by the spawnParticle (Explosion) function.
spawnExplosion :: World -> [Particle]
spawnExplosion world = take 500 (particleList (rndGen world))
    where particleList n = fst (spawnParticle Explosion world n) : particleList (snd (spawnParticle Explosion world n))


-------------------- BONUS --------------------
-- Returns a value which is fed into a Sin function. This is then used to scale the size of the bonus, resulting in a wave-like motion.
updateBonusScale :: Size -> Size
updateBonusScale s = s + 0.03

-- Returns an updated list of all the Bonus objects in the game world. It first checks if a Bonus was eaten or shot by the player. If so,
--  it will be excluded from the new Bonus list. If not, it is updated using updateBonusScale and added to the new Bonus list.
updateBonuses :: World -> [Bonus]
updateBonuses world = update (bonuses world)
    where update []     = []
          update (x:xs) | playerEatsBonus (playerPosition world) x = update xs
                        | bonusDestroyed  (bullets world)        x = update xs
                        | otherwise                                = (fst x, updateBonusScale (snd x)) : update xs

-- Spawns a Bonus object at a random position.
spawnBonus :: StdGen -> Bonus
spawnBonus rand = (randomPosition rand 420 250, 1)

-- Returns true if the player collided with a bonus object.
playerEatsBonus :: Position -> Bonus -> Bool
playerEatsBonus p b = dist < 20
    where dist = magV $ (fst b) - p

-- Returns true if the player shot a bonus object.
bonusDestroyed :: [Bullet] -> Bonus -> Bool
bonusDestroyed [] _     = False
bonusDestroyed l  bonus = any (==True) list
    where list            = map collides l
          collides bullet = magV ((fst bullet) - (fst bonus)) < 20

-- Returns the amount of Bonuses that a player ate in one frame.
bonusesEaten :: Position -> [Bonus] -> Int
bonusesEaten _ []     = 0
bonusesEaten p (x:xs) | playerEatsBonus p x = 1 + bonusesEaten p xs
                      | otherwise           = bonusesEaten p xs

-------------------- SCORE --------------------
-- Returns the updated score of a player. It is calculated by multiplying the amount of died enemies in 1 frame by the current
--  score Multiplier.
updateScore :: World -> Int
updateScore world = (score world) + length (enemiesDied world) * (multiplier world)

-- Returns the updated multiplier.
updateMultiplier :: World -> Int
updateMultiplier world = multiplier world + bonusesEaten (playerPosition world) (bonuses world)




